// 어플리케이션의 구조를 담당할 템플릿

import React from "react";

const Template = ({children}) => {
    return (
        <div>
            <div>오늘의 할 일(0)</div>
            <div>{children}</div>
        </div>
    );
};

export default Template;