import React, {useState} from "react";
import {MdAddCircle} from "react-icons/md";
import "./TodoInsert.css"

const TodoInsert = ({onInserToggle, onInsertTodo}) => {
    const [value,setValue] = useState("");
    //onChange는 input이 변경될 때마다 실행된다
    const onChange = (e) => {
        setValue(e.target.value);
    };

    const onSubmit =(e)=>{
        e.preventDefault()
        onInsertTodo(value);
        setValue(""); // 초기화시켜줌
        onInserToggle();
    }
  return (
    <div>
      <div className="background" onClick={onInserToggle}></div>
      <form onSubmit={onSubmit}>
        <input placeholder="please type" value={value} onChange={onChange}/>
        <button type="submit">
            <MdAddCircle/>
        </button>
      </form>
    </div>
  );
};

export default TodoInsert;
